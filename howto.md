---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Skiptir

[Skiptir](https://gitlab.com/icelandic-lt/nlp/skiptir) er orðskiptingatól fyrir íslensku sem er notað innan verkefna SÍM (Samstarf um íslenska máltækni). Tólið er notað til að skipta orðum eftir [nýjustu orðskiptingamynstrum fyrir íslensku](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/86).  
[Vefviðmót](http://malvinnsla.arnastofnun.is/ordskipting) fyrir tólið er einnig í boði.

## Hvað er orðskipting?

Orðskipting skilgreinir hvar má skipta orðum í hluta, t.d. milli lína:  
![orð-skipt-ing](/assets/img/mynd1.png)  
Þessar skilgreiningar eru notaðar í ýmsum tólum, m.a. fyrir leiðréttingu stafsetningar og málfars.

### Uppsetning:

Tólið (og Pyphen sem það byggir á) er skrifað í Python 3 og því þarf að vera með það uppsett.
Flest stýrikerfi í dag nema Windows eru þegar með Python uppsett.
Hægt er að ná í Python fyrir Windows [hér](https://www.python.org/downloads/windows/)  
Eftirfarandi notkunarleiðbeiningar gera þó ráð fyrir Linux.

Hægt er að sækja tólið í þjappaðari skrá af [GitLab síðunni](https://gitlab.com/icelandic-lt/nlp/skiptir), eða með skipanalínutólinu `git`:

`git clone https://gitlab.com/icelandic-lt/nlp/skiptir.git`

### Notkun:

`./skiptir.py [--mode MODE] [--hyphen HYPHEN]`  
Tólið les frá inntaki (e. standard input) og prentar orðskiptan texta í úttaki (e. standard output).  

MODE (í. háttur) er sjálfgefinn sem 'pattern', sem notar [Pyphen](https://github.com/Kozea/Pyphen) með nýjustu orðskiptingamynstrum fyrir íslensku.  
Aðrir hættir eru ekki studdir eins og er.  

HYPHEN (í. bandstrik) er sérvalið skiptingatákn, t.d. · eða -.  
Sjálfgefið skiptingatákn er skiptivísir ([e. soft hyphen](https://en.wikipedia.org/wiki/Hyphen#Soft_and_hard_hyphens)) (U+00AD).

### Dæmi:

Einfalt prufudæmi er að keyra tólið í skipanalínu:  
`./skiptir.py`  
og skrifa inn texta og ýta á enter, og ljúka svo með lausnarrununni (e. escape sequence) `CTRL + d`.  
![dæmi 1](/assets/img/skiptir1.png)  

Tólið er þó hannað til að taka við textaskrám og prentar þá skipt úttak textaskrár í skipanagluggann. Skráin input.txt fylgir með tólinu og er notuð hér sem dæmi:  
`./skiptir.py < input.txt`  
![dæmi 1](/assets/img/skiptir2.png) 

Einnig er hægt að beina úttaki í skrá (og skoða skrána svo, t.d. með tólinu `cat`):  
`./skiptir.py < input.txt > output.txt`  
![dæmi 1](/assets/img/skiptir3.png) 

Til að nota annað skiptingatákn, t.d. stjörnu (*), er `--hyphen` stiki notaður:  
`./skiptir.py --hyphen '*'`  
![dæmi 1](/assets/img/skiptir4.png) 