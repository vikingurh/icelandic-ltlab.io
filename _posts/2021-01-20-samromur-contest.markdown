---
layout: post
title: Grunnskólakeppni Samróms
description: '' 
date: 2021-01-20 09:00:07
hero_image: img/samromur.png
hero_height: is-large
hero_darken: true
image: img/samromur.png
tags: Samrómur Lestrarkeppni
#canonical_url: https://www.csrhymes.com/2021/01/20/samromur-school-competition.html
---

Þann 18. janúar fór af stað önnur Lestrarkeppni grunnskóla ar sem keppt er um fjölda setninga sem nemendur lesa inn á síðunni samrómur.is. 
Forsetahjónin, Guðni Th. Jóhannesson og Eliza Reid, sendu kveðju og hvöttu til þátttöku, eins og sá má [hér](https://www.facebook.com/samromur/videos/424360035575420)

„Áfram söfnum við röddum Íslendinga til þess að tæki okkar og tölvur skilji íslensku, við viljum forðast það að þurfa tala ensku eða önnur mál á þeim vettvangi. Framtíð tungumálsins veltur á því að við getum notað það í stafrænum heimi - tungumálið okkar er einstakt og við eigum það saman“

Keppnin fór af stað af ótrúlegum krafti og hafa aldrei verið lesnar inn eins margar setningar á jafn stuttum tíma og fyrsta sólarhring keppninnar! Keppnin stendur til 25. janúar, allir geta tekið þátt á [vefsíðu Samróms](https://samromur.is/)
